#!/bin/bash

if [ ! -d /opt/jellyfin/config ] || [ ! -d /opt/jellyfin/data/tvshows ] || [ ! -d /opt/jellyfin/data/movies ]; then
  mkdir -p /opt/jellyfin/config /opt/jellyfin/data/tvshows /opt/jellyfin/data/movies 
fi

docker run \
  -d --restart=always \
  --name=jellyfin \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e UMASK_SET=022 `#optional` \
  -p 8096:8096 \
  -p 8920:8920 `#optional` \
  -v /path/to/library:/config \
  -v /path/to/tvseries:/data/tvshows \
  -v /path/to/movies:/data/movies \
  linuxserver/jellyfin
