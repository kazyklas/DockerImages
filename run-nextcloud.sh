#!/bin/bash

if [ ! -d /opt/nextcloud/appdata ]; then
   mkdir -p /opt/nextcloud/appdata
fi

if [ ! -d /opt/nextcloud/data ]; then
   mkdir -p /opt/nextcloud/data
fi

# service will be available at https://localhost:443

docker run \
  -d --restart=always \
  --name=nextcloud \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -p 443:443 \
  -v /opt/nextcloud/appdata:/config \
  -v /opt/nextcloud/data:/data \
  linuxserver/nextcloud
