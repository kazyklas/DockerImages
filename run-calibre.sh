#!/bin/bash

if [ ! -d /opt/calibre/config ]; then
	mkdir -p /opt/calibre/config
fi

# service will be wailable at http://localhost:8080

sudo docker run \
  -d --restart=always \
  --name=calibre \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/Prague \
  -p 8080:8080 \
  -p 8081:8081 \
  -v /opt/calibre/config:/config \
  linuxserver/calibre
