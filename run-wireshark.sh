#!/bin/bash 

if [ ! -d /opt/wireshark/config ]; then
	mkdir -p /opt/wireshark/config
fi

# service will be available at http://localhost:3000

docker run \
  -d --restart=always \
  --name=wireshark \
  --net=host \
  --cap-add=NET_ADMIN \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -p 3000:3000  \
  -v /path/to/config:/config \
  linuxserver/wireshark
